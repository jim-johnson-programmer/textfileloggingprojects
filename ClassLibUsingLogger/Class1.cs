﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using logLib=Jc.Library.Log.TextFile.RollingFileAppender;

namespace ClassLibUsingLogger
{
    public class Class1
    {
        ///put code here to demo how to do trx in other libs 
        public void RunLogger()
        {
          logLib.ILogger myLog = new logLib.Logger(typeof(Class1));
          myLog.LogError("ClassLibUsingLogger error.");
        }
    }
}

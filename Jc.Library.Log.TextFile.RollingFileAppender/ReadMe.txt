﻿This effort has some key identifcation items
1.) Log4net dll is added from nuget feed in all projects that will use the logger
2.) Log4Net dll is added to this library via nuget
3.) Put settings file called in this case log4net.config.  Put that file in this library
4.) Init code for file is in AssemblyInfo.cs of that prject.
5.) Put Interface ILogger.cs and Logger.cs in this library project 
6.) Put in view model method to pull together all var names and values, and write to log.

http://hirenkhirsaria.blogspot.com/2012/06/logging-using-log4net-in-wpf.html


Code to use logging
          using log4net;  //goes with using log4net dll
          using logLib=Jc.Library.Log.TextFile.RollingFileAppender; //class lib for logging
           
                                                        Class1=name of class hosting this 
          logLib.ILogger myLog = new logLib.Logger(typeof(Class1));
          myLog.LogError("My logging message");
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
How to load new project in BitBucket
1.) Log into BitBucket
2.) Click "Clone" 
3.) In ajax pop up, change drop down to Https from SSH.
4.) Copy htts address without command for GIT. Paste into notepad or editor to keep handy.

Sourcetree portion....

5.) Click "Clone Repository". Switch to "Add working copy" tab.
7.) Open GIT GUI tool.  Click "Create new repository".
8.) Nav to directory at top of repos, and click to select it. Folder now is Init as GIT repos.
8.) Paste full path from 8 into source tree path for top root of src of code.
9.) Src tree will check folder is GIT repos and enable create button if src tree believes it to be a GIT repos via Init() command.
10.) Click OK and create local repos.

Add Remote and push code to it...
11.) With new repos selected, click "Repository" menu, and choose "Repository Settings" menu item.
12.) REMOTES ARE ONLY ASSOCIATED WITH CURRENT REPOSITORY. NO REMOTES ARE SHARED ACROSS MULTIPLE REPOSITORIES.
13.) In dialog box, in remotes tab, click "Add"
14.) First textbox is for remote name which should be "origin". Click checkbox to make "default".
15.) Select "Bitbucket" from Hosttype dropdown.
16.) Second textbox is for HTTPS path from Bitbucket. From step 4 above. Paste in and watch user name autopopulate from pasted https path.
17.) Save out remote info.

First commit and push to remote....
18.) Until local branch is created with first commit, remote will show as a dash in dialog. 
19.) Do a local commit. Master branch is created. Stage files and give a commit description.
20.) Do a push, and upload to master on remote.

Create Dev branch via GIT Flow...
21.) Click GIT Flow and wait while Sourcetree unfreezes in couple of minutes.
22.) Take defaults from dialog, click OK. (Will not work with uncommitted changes.)


